# idup

The Intuitive DUB.

Currently implemented:
- Nothing

Design goals:
- Compatibility with everything that makes DUB great: `dub.json`, the DUB registry.
- Simple capabilities. We want to limit powerfulness to reap the benefit of being declarative.
- Manageable complexity: should stay easy to get into the codebase.
- User-friendly: minimal number of commands, colors.
- Do not check network by default.
- Think about easier private packages
- Remove features from DUB:
  * single-file dependencies
  * overrides
  * manual fetch/remove
  * registry search
  * strict JSON parsing, but with warnings
  * use the `dub.json` format exclusively, no `dub.sdl`
  * "purer" API, don't pass around the state machine in the library code
  * separate dependency resolution from building

