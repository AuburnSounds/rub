module main;

import std.stdio;
import std.path;
import std.file;
import std.string;
import utils;
import help;
import colorize;
import recipe;

import standardpaths;

int main(string[] args)
{
    try
    {
        // Drop executable path
        args = args[1..$];

        // What is the first argument?
        string firstArg = "run";
        if (args.length > 0)
            firstArg = args[0];

        if (firstArg == "help")
        {
            string topic = args.length > 1 ? args[1] : "idub";
            displayHelp(topic);
        }
        else if (firstArg == "registry")
        {
            import std.process;
            browse("https://code.dlang.org/my_packages");
        }
        else if (firstArg == "run")
        {
            readRecipeFromCwd();
            //displayHelp("run"); // TODO
        }
        else if (firstArg == "build")
        {
            displayHelp("build"); // TODO
        }
        else if (firstArg == "upgrade")
        {
            auto configPath = ensureSomeBasicConfiguration();
            updateRegistryData(configPath, true );
            getRegistryPackageJSON(configPath, true, "dplug");
            getRegistryPackageJSON(configPath, true, "gfm");
            getRegistryPackageJSON(configPath, true, "derelict-util");
           // displayHelp();
        }
        else
        {
            errorc(format("Unknown command %s", firstArg.yellow));
            displayHelp("idub"); // display regular help
            throw new Exception("Try one of the existing commands");
        }
        return 0;
    }
    catch(Exception e)
    {
        errorc(e.msg);
        cwriteln;
        return 1;
    }
}

string ensureSomeBasicConfiguration()
{
    // Ensure some configuration is always there
    auto configPath = getConfigPath();
    mkdirRecurse(buildPath(configPath, "registry"));
    mkdirRecurse(buildPath(configPath, "packages"));
    return configPath;
}


// Return a lazily created path for all cached data
string getConfigPath()
{
    string configDir = writablePath(StandardPath.config, "rub", FolderFlag.create);
    if (configDir.length)
        return configDir;
    else
        throw new Exception("Could not create rub.conf in config directory " ~ configDir);
}

// Lazily return the API
string getRegistryPackageJSON(string configPath, bool forceInvalidateCache, string packageName)
{
    string jsonFilename = packageName ~ ".json";
    string cacheFile = buildPath(configPath, "registry", jsonFilename);
    return downloadOrUseCache("http://code.dlang.org/api/packages/" ~ packageName ~ "/info", cacheFile, forceInvalidateCache);
}

string updateRegistryData(string configPath, bool forceInvalidateCache)
{
    string cacheFile = buildPath(configPath, "registry", "index.json");
    return downloadOrUseCache("http://code.dlang.org/packages/index.json", cacheFile, forceInvalidateCache);
}
