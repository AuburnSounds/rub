module recipe;

import std.array : empty;
import std.conv;
import std.algorithm : canFind;
import std.file;
import std.path;
import std.stdio;
import std.string;
import std.exception : enforce;

import permissivejson;
import dependency;
import utils;

struct PackageRecipe
{
    // Specified if this package was parsed from file
    // Is not set for subpackages
    string filename;
    // set for subpackages
    string path;
    string version_;

    string name;
    string description;
    string minDubVersion;
    string homepage;
    string[] authors;
    string copyright;
    string license;
    string[] ddoxFilterArgs;
    string ddoxTool;
    PackageRecipe[] subPackages;
    BuildSettings buildSettings;
    PackageConfiguration[] configurations;
    BuildSettings[string] buildTypes;
}

struct PackageConfiguration
{
    string name;
    string[] platforms;
    BuildSettings buildSettings;
}

enum TargetType
{
    autodetect,
    none,
    executable,
    library,
    sourceLibrary,
    staticLibrary,
    dynamicLibrary
}

struct BuildSettings
{
    Dependency[string] dependencies;
    string systemDependencies;
    TargetType targetType;
    string targetName;
    string targetPath;
    string workingDirectory;
    string[string] subConfigurations;
    string[] buildRequirements;
    string[] buildOptions;
    string[] libs;
    string[] sourceFiles;
    string[] sourcePaths;
    string[] excludedSourceFiles;
    string mainSourceFile;
    string[] copyFiles;
    string[] versions;
    string[] debugVersions;
    string[] importPaths;
    string[] stringImportPaths;
    string[] preGenerateCommands;
    string[] postGenerateCommands;
    string[] preBuildCommands;
    string[] postBuildCommands;
    string[] dflags;
    string[] lflags;
}


void readRecipeFromCwd()
{
    string dir = getcwd();
    string filename = buildPath(dir, "dub.json");
    if (!exists(filename))
        throw new Exception(format("No dub.json found in\n'%s'", filename));
    readFromFile(filename);
}

PackageRecipe readFromFile(string filename)
{
    writefln("readFromFile %s", filename);
    auto source = cast(string)read(filename);
    JSONValue root = parseJSONPermissive(source, filename);

    PackageRecipe result;
    result.filename = filename;
    fromJson(result, root, null);
    return result;
}

void fromJson(ref PackageRecipe recipe, ref JSONValue root, string parentName)
{
    foreach (string item, ref JSONValue value; root)
    switch (item)
    {
        case "name": recipe.name = value.str; break;
        //case "version": recipe.version_ = value.str; break;
        case "description": recipe.description = value.str; break;
        case "minDubVersion": recipe.minDubVersion = value.str; break;
        case "homepage": recipe.homepage = value.str; break;
        case "authors": recipe.authors = parseStringArray(value); break;
        case "copyright": recipe.copyright = value.str; break;
        case "license": recipe.license = value.str; break;
        case "buildTypes":
            foreach (string name, settings; value) {
                BuildSettings bs;
                bs.fromJson(value, null);
                recipe.buildTypes[name] = bs;
            }
            break;
        case "-ddoxFilterArgs": recipe.ddoxFilterArgs = parseStringArray(value); break;
        case "-ddoxTool": recipe.ddoxTool = value.str; break;
        default: break;
    }

    string fullname = parentName.length != 0 ? parentName ~ ":" ~ recipe.name : recipe.name;

    recipe.buildSettings.fromJson(root, fullname);

    if (auto configurations = "configurations" in root)
    {
        recipe.configurations = new PackageConfiguration[configurations.array.length];
        foreach (size_t i, JSONValue configuration; configurations.array)
        {
            foreach (string item, ref JSONValue value; configuration)
            switch (item)
            {
                case "name": recipe.configurations[i].name = value.str; break;
                case "platforms": recipe.configurations[i].platforms = parseStringArray(value); break;
                default: break;
            }
            recipe.configurations[i].buildSettings.fromJson(configuration, fullname);
        }
    }

    if (auto subPackages = "subPackages" in root)
    {
        recipe.subPackages = new PackageRecipe[subPackages.array.length];
        foreach (size_t i, JSONValue subPackage; subPackages.array)
        {
            if (subPackage.type == JSONType.object)
                fromJson(recipe.subPackages[i], subPackage, fullname);
            else
                recipe.subPackages[i].path = subPackage.str;
        }
    }
}

void fromJson(ref BuildSettings s, ref JSONValue root, string package_name)
{
    foreach (string item, ref JSONValue value; root)
    {
        auto idx = indexOf(item, "-");
        string basename;
        string suffix;

        if( idx >= 0 ) {
            basename = item[0 .. idx];
            suffix = item[idx .. $];
        } else
            basename = item;

        switch (item)
        {
            case "dependencies":
                foreach (string pkg, verspec; value) {
                        if (pkg.startsWith(":")) {
                            enforce(!package_name.canFind(':'),
                                format("Short-hand packages syntax not allowed within sub packages: %s -> %s",
                                    package_name, pkg));
                            pkg = package_name ~ pkg;
                        }
                        enforce(pkg !in s.dependencies, "The dependency '"~pkg~"' is specified more than once." );
                        s.dependencies[pkg] = Dependency.fromJson(verspec);
                }
                break;
            case "systemDependencies": s.systemDependencies = value.str; break;
            case "targetType": s.targetType = value.str.to!TargetType; break;
            case "targetName": s.targetName = value.str; break;
            case "targetPath": s.targetPath = value.str; break;
            case "workingDirectory": s.workingDirectory = value.str; break;
            case "subConfigurations":
                foreach (string key, ref JSONValue elem; value.object)
                    s.subConfigurations[key] = elem.str;
                break;
            case "buildRequirements": s.buildRequirements = parseStringArray(value); break;
            case "buildOptions": s.buildOptions = parseStringArray(value); break;
            case "libs": s.libs = parseStringArray(value); break;
            case "sourceFiles": s.sourceFiles = parseStringArray(value); break;
            case "sourcePaths": s.sourcePaths = parseStringArray(value); break;
            case "excludedSourceFiles": s.excludedSourceFiles = parseStringArray(value); break;
            case "mainSourceFile": s.mainSourceFile = value.str; break;
            case "copyFiles": s.copyFiles = parseStringArray(value); break;
            case "versions": s.versions = parseStringArray(value); break;
            case "debugVersions": s.debugVersions = parseStringArray(value); break;
            case "importPaths": s.importPaths = parseStringArray(value); break;
            case "stringImportPaths": s.stringImportPaths = parseStringArray(value); break;
            case "preGenerateCommands": s.preGenerateCommands = parseStringArray(value); break;
            case "postGenerateCommands": s.postGenerateCommands = parseStringArray(value); break;
            case "preBuildCommands": s.preBuildCommands = parseStringArray(value); break;
            case "postBuildCommands": s.postBuildCommands = parseStringArray(value); break;
            case "dflags": s.dflags = parseStringArray(value); break;
            case "lflags": s.lflags = parseStringArray(value); break;
            default: break;
        }
    }
}

string[] parseStringArray(ref JSONValue arr)
{
    string[] result = new string[arr.array.length];
    foreach (size_t i, ref JSONValue elem; arr)
    {
        result[i] = elem.str;
    }
    return result;
}

// Test global settings
unittest
{
    auto json =
`{
    "name" : "testname",
    "description" : "test description",
    "minDubVersion" : "1.0.0",
    "homepage" : "test.page",
    "authors" : ["author 1"],
    "copyright" : "Copyright",
    "license" : "BSL-1.0"
}`;

    JSONValue root = parseJSON(json);
    PackageRecipe r;
    fromJson(r, root, null);

    //r.toJson.writeln;

    assert(r.name == "testname");
    assert(r.description == "test description");
    assert(r.minDubVersion == "1.0.0");
    assert(r.homepage == "test.page");
    assert(r.authors == ["author 1"]);
    assert(r.copyright == "Copyright");
    assert(r.license == "BSL-1.0");
    // TODO '-ddoxFilterArgs'
}

// Test sub packages
unittest
{
    auto json =
`{
    "name" : "testname",
    "subPackages": [
        "./component1/",
        "./component2/",
        {
            "name": "component3",
            "targetType": "library",
            "sourcePaths": ["component1/source"],
            "importPaths": ["component1/source"]
        }
    ]
}`;

    JSONValue root = parseJSONPermissive(json, "unittest-recipe-L"~__LINE__.to!string);
    PackageRecipe r;
    fromJson(r, root, null);

    //r.toJson.writeln;

    assert(r.name == "testname");
    assert(r.subPackages.length == 3);
    assert(r.subPackages[0].path == "./component1/");
    assert(r.subPackages[0].name == null);
    assert(r.subPackages[1].path == "./component2/");
    assert(r.subPackages[1].name == null);
    assert(r.subPackages[2].name == "component3");
    assert(r.subPackages[2].path == null);
}

// Test build settings
unittest
{
    auto json =
`{
    "name" : "test",
    "dependencies" : {
        "dep1" : "==1.3.0",
        "dep2" : {
            "version" : "==2.3.4",
            "optional": true,
            "default": true
        },
        "dep3" : {
            "path": "dep3path"
        },
        "test:sub1" : "*"
    },
    "systemDependencies" : "systemDependencies text",
    "targetType" : "library",
    "targetName" : "targetName text",
    "targetPath" : "some path",
    "workingDirectory" : "workingDirectory path",
    "subConfigurations" : {
        "dep1" : "subconfig1",
        "test:sub1" : "subconfig2"
    },
    "buildRequirements" : ["allowWarnings", "silenceWarnings", "disallowDeprecations"],
    "buildOptions" : ["debugMode", "coverage", "debugInfo"],
    "libs" : ["ssl", "curl"],
    "sourceFiles" : ["main.d"],
    "sourcePaths" : ["extra"],
    "excludedSourceFiles" : ["util_main.d"],
    "mainSourceFile" : "main.d",
    "copyFiles" : ["libs/*"],
    "versions" : ["ver1", "ver2"],
    "debugVersions" : ["dver1", "dver2"],
    "importPaths" : ["imppath1", "imppath2"],
    "stringImportPaths" : ["strimppath1", "strimppath2"],
    "preGenerateCommands" : ["pregcom1", "pregcom2"],
    "postGenerateCommands" : ["postgcom1", "postgcom2"],
    "preBuildCommands" : ["prebcom1", "prebcom2"],
    "postBuildCommands" : ["postbcom1", "postbcom2"],
    "dflags" : ["dflag1", "dflag2"],
    "lflags" : ["lflag1", "lflag2"]
}`;

    JSONValue root = parseJSONPermissive(json, "unittest-recipe-L"~__LINE__.to!string);
    PackageRecipe r;
    fromJson(r, root, null);

    //r.toJson.writeln;

    assert(r.name == "test");
    assert(r.buildSettings.dependencies.length == 4);

    assert(r.buildSettings.dependencies["dep1"] == Dependency("==1.3.0"));

    auto dep2 = Dependency("==2.3.4");
    dep2.optional = true;
    dep2.default_ = true;
    assert(r.buildSettings.dependencies["dep2"] == dep2);

    assert(r.buildSettings.dependencies["dep3"] == Dependency(NativePath("dep3path")));
    assert(r.buildSettings.dependencies["test:sub1"] == Dependency.any);

    assert(r.buildSettings.targetType == TargetType.library);
    assert(r.buildSettings.targetName == "targetName text");
    assert(r.buildSettings.targetPath == "some path");
    assert(r.buildSettings.workingDirectory == "workingDirectory path");
    assert(r.buildSettings.subConfigurations == ["dep1" : "subconfig1", "test:sub1" : "subconfig2"]);
    assert(r.buildSettings.buildRequirements == ["allowWarnings", "silenceWarnings", "disallowDeprecations"]);
    assert(r.buildSettings.buildOptions == ["debugMode", "coverage", "debugInfo"]);
    assert(r.buildSettings.libs == ["ssl", "curl"]);
    assert(r.buildSettings.sourceFiles == ["main.d"]);
    assert(r.buildSettings.sourcePaths == ["extra"]);
    assert(r.buildSettings.excludedSourceFiles == ["util_main.d"]);
    assert(r.buildSettings.mainSourceFile == "main.d");
    assert(r.buildSettings.copyFiles == ["libs/*"]);
    assert(r.buildSettings.versions == ["ver1", "ver2"]);
    assert(r.buildSettings.debugVersions == ["dver1", "dver2"]);
    assert(r.buildSettings.importPaths == ["imppath1", "imppath2"]);
    assert(r.buildSettings.stringImportPaths == ["strimppath1", "strimppath2"]);
    assert(r.buildSettings.preGenerateCommands == ["pregcom1", "pregcom2"]);
    assert(r.buildSettings.postGenerateCommands == ["postgcom1", "postgcom2"]);
    assert(r.buildSettings.preBuildCommands == ["prebcom1", "prebcom2"]);
    assert(r.buildSettings.postBuildCommands == ["postbcom1", "postbcom2"]);
    assert(r.buildSettings.dflags == ["dflag1", "dflag2"]);
    assert(r.buildSettings.lflags == ["lflag1", "lflag2"]);
}

JSONValue toJson(ref PackageRecipe recipe)
{
    JSONValue root;
    root["name"] = recipe.name;
    if (recipe.description) root["description"] = recipe.description;
    if (recipe.homepage) root["homepage"] = recipe.homepage;
    if (recipe.authors) root["authors"] = recipe.authors.toJson;
    if (recipe.copyright) root["copyright"] = recipe.copyright;
    if (recipe.license) root["license"] = recipe.license;
    if (!recipe.subPackages.empty)
    {
        JSONValue[] subPackages = new JSONValue[recipe.subPackages.length];
        foreach (i, sub; recipe.subPackages)
            subPackages[i] = toJson(sub);
        root["subPackages"] = subPackages;
    }
    if (recipe.configurations.length) {
        JSONValue[] configs = new JSONValue[recipe.configurations.length];
        foreach(i, config; recipe.configurations)
            configs[i] = config.toJson();
        root["configurations"] = configs;
    }
    if (recipe.buildTypes.length) {
        JSONValue[string] types;
        foreach (name, settings; recipe.buildTypes) {
            JSONValue settingsJson;
            settings.toJson(settingsJson);
            types[name] = settingsJson;
        }
        root["buildTypes"] = types;
    }
    recipe.buildSettings.toJson(root);

    return root;
}

JSONValue toJson(ref PackageConfiguration config)
{
    JSONValue root;
    root["name"] = config.name;
    if (config.platforms.length) root["platforms"] = config.platforms.toJson;
    config.buildSettings.toJson(root);
    return root;
}

void toJson(ref BuildSettings s, ref JSONValue root)
{
    if(s.dependencies !is null){
        JSONValue[string] deps;
        foreach(pack, dep; s.dependencies)
            deps[pack] = dep.toJson;
        root["dependencies"] = deps;
    }
    if (s.systemDependencies) root["systemDependencies"] = s.systemDependencies;
    if (s.targetType) root["targetType"] = s.targetType.to!string;
    if (s.targetName) root["targetName"] = s.targetName;
    if (s.targetPath) root["targetPath"] = s.targetPath;
    if (s.workingDirectory) root["workingDirectory"] = s.workingDirectory;
    if (s.subConfigurations) {
        JSONValue subConfigurations;
        foreach (string key, string value; s.subConfigurations)
            subConfigurations[key] = value;
        root["subConfigurations"] = subConfigurations;
    }
    if (s.buildRequirements) root["buildRequirements"] = s.buildRequirements.toJson;
    if (s.buildOptions) root["buildOptions"] = s.buildOptions.toJson;
    if (s.libs) root["libs"] = s.libs.toJson;
    if (s.sourceFiles) root["sourceFiles"] = s.sourceFiles.toJson;
    if (s.sourcePaths) root["sourcePaths"] = s.sourcePaths.toJson;
    if (s.excludedSourceFiles) root["excludedSourceFiles"] = s.excludedSourceFiles.toJson;
    if (s.mainSourceFile) root["mainSourceFile"] = s.mainSourceFile;
    if (s.copyFiles) root["copyFiles"] = s.copyFiles.toJson;
    if (s.versions) root["versions"] = s.versions.toJson;
    if (s.debugVersions) root["debugVersions"] = s.debugVersions.toJson;
    if (s.importPaths) root["importPaths"] = s.importPaths.toJson;
    if (s.stringImportPaths) root["stringImportPaths"] = s.stringImportPaths.toJson;
    if (s.preGenerateCommands) root["preGenerateCommands"] = s.preGenerateCommands.toJson;
    if (s.postGenerateCommands) root["postGenerateCommands"] = s.postGenerateCommands.toJson;
    if (s.preBuildCommands) root["preBuildCommands"] = s.preBuildCommands.toJson;
    if (s.postBuildCommands) root["postBuildCommands"] = s.postBuildCommands.toJson;
    if (s.dflags) root["dflags"] = s.dflags.toJson;
    if (s.lflags) root["lflags"] = s.lflags.toJson;
}

JSONValue[] toJson(string[] array)
{
    JSONValue[] result = new JSONValue[array.length];
    foreach(i, elem; array)
        result[i] = JSONValue(elem);
    return result;
}