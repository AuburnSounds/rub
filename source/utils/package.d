module utils;

public import utils.path;
public import utils.semver;

import std.process;
import std.string;
import std.file;
import std.path;

import colorize;
import http2;

string white(string s) @property
{
    return s.color(fg.light_white);
}

string grey(string s) @property
{
    return s.color(fg.white);
}

string cyan(string s) @property
{
    return s.color(fg.light_cyan);
}

string green(string s) @property
{
    return s.color(fg.light_green);
}

string yellow(string s) @property
{
    return s.color(fg.light_yellow);
}

string red(string s) @property
{
    return s.color(fg.light_red);
}

void infoc(string msg)
{
    cwritefln("info: ".white ~ "%s.", msg);
}

void warningc(string msg)
{
    cwritefln("warning: ".yellow ~ "%s.", msg);
}

void errorc(string msg)
{
    cwritefln("error: ".red ~ "%s.", msg);
}

class ExternalProgramErrored : Exception
{
    public
    {
        @safe pure nothrow this(int errorCode,
                                string message,
                                string file =__FILE__,
                                size_t line = __LINE__,
                                Throwable next = null)
        {
            super(message, file, line, next);
            this.errorCode = errorCode;
        }

        int errorCode;
    }
}


void safeCommand(string cmd)
{
    cwritefln("$ %s".cyan, cmd);
    auto pid = spawnShell(cmd);
    auto errorCode = wait(pid);
    if (errorCode != 0)
        throw new ExternalProgramErrored(errorCode, format("Command '%s' returned %s", cmd, errorCode));
}

// Currently this only escapes spaces...
string escapeShellArgument(string arg)
{
    version(Windows)
    {
        return `"` ~ arg ~ `"`;
    }
    else
        return arg.replace(" ", "\\ ");
}

/// Recursive directory copy.
/// https://forum.dlang.org/post/n7hc17$19jg$1@digitalmars.com
/// Returns: number of copied files
int copyRecurse(string from, string to, bool verbose)
{
  //  from = absolutePath(from);
  //  to = absolutePath(to);

    if (isDir(from))
    {
        if (verbose) cwritefln("    => Create directory %s".green, to);
        mkdirRecurse(to);

        auto entries = dirEntries(from, SpanMode.shallow);
        int result = 0;
        foreach (entry; entries)
        {
            auto dst = buildPath(to, entry.name[from.length + 1 .. $]);
            result += copyRecurse(entry.name, dst, verbose);
        }
        return result;
    }
    else
    {
        if (verbose) cwritefln("    => Copy %s to %s".green, from, to);
        std.file.copy(from, to);
        return 1;
    }
}

string downloadOrUseCache(string url, string cacheFile, bool forceCacheInvalidation)
{
    import std.file;

    bool updateFile = forceCacheInvalidation || !exists(cacheFile);
    if (updateFile)
    {
        auto client = new HttpClient(); // TODO lazy web client, or lazy client pools
        auto request = client.navigateTo(Uri(url));
        auto response = request.waitForCompletion();
        auto content = response.contentText;
        std.file.write(cacheFile, response.contentText);
        return content;
    }
    else
        return cast(string)( std.file.read(cacheFile) );
}