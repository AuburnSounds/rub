module help;

import std.string;

import colorize;
import utils;

static immutable string[] allTopics =
[    
    "build",
    "clean",
    "dup",
    "help",
    "registry",
    "run",
    "upgrade"
];

void displayHelp(string topic)
{
    cwriteln;
    if (topic == "dup")
    {
        displayHelpDup();
    }
    else if (topic == "help")
    {
        displayHelphelp();
    }
    else if (topic == "registry")
    {
        displayHelpRegistry();
    }
    
    else
    {
        throw new Exception(format("Unknown topic %s. Type %s to see available topics", 
                                   topic.yellow, 
                                   "dup help help".cyan));
    }
}

void displayHelpRegistry()
{
    cwriteln("    USAGE ".white);
    cwriteln("        dup registry".cyan);
    cwriteln;
    cwriteln("    DESCRIPTION".white);
    cwriteln("        Opens a browser to the DUB registry.");
    cwriteln("        Exact URL is https://code.dlang.org/my_packages.");
    cwriteln;
}

void displayHelphelp()
{
    cwriteln("    USAGE ".white);
    cwriteln(("        dup help [" ~ "topic".yellow ~ "]".cyan).cyan);
    cwriteln;
    cwriteln("    AVAILABLE TOPICS ".white);
    foreach(t; allTopics)
        cwriteln("        " ~ t.yellow);
    cwriteln;
}

void displayHelpDup()
{
    cwriteln("    USAGE".white);
    cwriteln(("        dup [" ~ "command".yellow ~ "]".cyan).cyan);
    cwriteln;
    cwriteln("    COMMANDS".white);


    void flag(string f, string desc)
    {
        cwrite("        ");
        cwrite(f.yellow);
        for(auto i = f.length; i < 16; ++i)
            cwrite(" ");
        cwriteln(desc.grey);
    }

    cwriteln;
    flag("build", "Build a package without using the network");
    flag("clear", "Clear global DUP artifacts");
    flag("help [topic]", "Show this help (" ~ "dup help help".cyan ~ " to list topics)");
    flag("registry", "Browse to the DUB registry");
    flag("run",   "Build and also run a package");
    flag("upgrade", "Resolve and checkout dependencies, uses network");
    cwriteln;    
}