/// Permissive JSON parser, warns about trailing commas.
/// Provides line and column information.
/// Probably parses Javascript tokens too permissively.
module permissivejson;

import std.array;
import std.stdio;
import std.string;
import std.range;
import std.conv;
import std.utf;
import std.uni;

import utils: warningc;

public import std.json;

/// Parses JSON permissively, with friendly warnings.
JSONValue parseJSONPermissive(string source, string filename)
{
    Parser parser;
    parser.initialize(source, filename);
    JSONValue root = parser.parseValue();
    parser.ensureInputIsConsumed();
    return root;
}

struct Pos
{
    int line;
    int col;
}

enum TokenType
{
    invalid,
    number,
    string_,
    colon,                 // :
    comma,                 // ,
    openingBracket,        // [
    closingBracket,        // ]
    openingBrace,          // {
    closingBrace,          // }
    true_,
    false_,
    null_,
    inputTerminator,
}


struct Token
{
    TokenType type;
    wstring stringValue;
    double numberValue;
    Pos pos;

    string toString()
    {
        return tokenToString(type);
    }
}

static string tokenToString(TokenType type) pure nothrow
{
    final switch (type) with (TokenType)
    {
        case invalid: assert(false); // should never be emitted
        case number: return "number literal";
        case string_: return "string literal";
        case colon: return ":";
        case comma: return ",";
        case openingBracket: return "[";
        case closingBracket: return "]";
        case openingBrace: return "{";
        case closingBrace: return "}";
        case true_: return "true";
        case false_: return "false";
        case null_: return "null";
        case inputTerminator: return "EOF";
    }
}

/// Thrown when code did not lex.
class LexException : Exception
{
    public
    {
        @safe pure this(string file, int line, int column, string message)
        {
            string fmtMessage = format("%s(%d,%d): %s", file, line, column, message);
            super(fmtMessage, file, line, next);
            this.line = line;
            this.column = column;
            this.filename = file;
        }
    }
    int line;
    int column;
    string filename;
}

/// Thrown when code did not parse.
class ParseException : Exception
{
    public
    {
        @safe pure this(string file, Pos pos, string message)
        {
            this.line = pos.line;
            this.column = pos.col;
            this.filename = file;
            string fmtMessage = format("%s(%d,%d): %s", file, line, column, message);
            super(fmtMessage, file, line, next);
        }
    }
    int line;
    int column;
    string filename;
}

/// Javascript lexer
struct Lexer(Input) if (isInputRange!Input && is(ElementType!Input : dchar))
{
public:

    void initialize(Input input, string filename)
    {
        _input = input;
        _position = 0;
        _line = 1;
        _col = 1;
        _file = filename;

        _identifierBuffer = appender!(dchar[])();
        _identifierBuffer.reserve(256);
        _stringLiteralBuffer = appender!(ushort[])();
        _stringLiteralBuffer.reserve(2048);
    }

    Token nextToken()
    {
        void popChar()
        {
            _input.popFront();
            ++_col;
        }

        dchar front()
        {
            if (_input.empty)
                throw new LexException(_file, _line, _col, "Unexpected end of input");
            return _input.front();
        }

    initial:

        // The token, if any, would start here
        int tokenLine = _line;
        int tokenCol = _col;

        Token makeToken(TokenType type)
        {
            Token tok;
            tok.type = type;
            tok.pos.line = tokenLine;
            tok.pos.col = tokenCol;
            return tok;
        }

        // No more input
        if (_input.empty)
            return makeToken(TokenType.inputTerminator);

        dchar ch = front();

        void next()
        {
            popChar();
            ch = front();
        }

        int lexUnicodeEscape()
        {
            int codepoint = 0;

            if (ch == '{')
            {
                next();
                // \u{xxx} Unicode escape sequence

                // Always at least one Unicode character
                codepoint = codepoint * 16 + hexCharToInt(_line, _col, ch);
                next();

                while(ch != '}') // Arbitrary number of hexadecimal numbers
                {
                    codepoint = codepoint * 16 + hexCharToInt(_line, _col, ch);
                    if (codepoint > 0x10FFFF)
                        throw new LexException(_file, _line, _col, "Unicode codepoint is greater than 0x10FFFF");
                    next();
                }
                next();
            }
            else
            {
                for (int digits = 0; digits < 4; ++digits)
                {
                    codepoint = codepoint * 16 + hexCharToInt(_line, _col, ch);
                    next();
                }
            }
            return codepoint;
        }

        // <number-parsing>
        string lexDecimalDigits()
        {
            string value = "";
            if (!isDigit(ch))
                throw new LexException(_file, _line, _col, "Expected decimal digit in number literal");

            while(isDigit(ch))
            {
                value ~= ch;
                next();
            }
            return value;
        }

        string lexDecimalIntegerLiteral()
        {
            if (ch == '0')
            {
                next();
                return "0";
            }
            else
                return lexDecimalDigits();
        }

        string lexOptionalExponentPart()
        {
            string value = "";
            if (ch == 'e' || ch == 'E')
            {
                value ~= 'e';
                next();

                // lex SignedInteger
                if (ch == '+')
                {
                    value ~= ch;
                    next();
                }
                else if (ch == '-')
                {
                    value ~= ch;
                    next();
                }
                value ~= lexDecimalDigits();
            }
            return value;
        }

        string lexDecimalLiteralStartingWithDigit()
        {
            string value = lexDecimalIntegerLiteral();
            if (ch == '.')
            {
                value ~= '.';
                next();
                if (isDigit(ch))
                    value ~= lexDecimalDigits();
            }

            value ~= lexOptionalExponentPart();
            return value;
        }

        string lexDecimalLiteralStartingWithPoint()
        {
            string value = ".";
            value ~= lexDecimalDigits();
            value ~= lexOptionalExponentPart();
            return value;
        }
        // </number-parsing>

        // skip whitespace
        if (isWhitespace(ch))
        {
            popChar();
            goto initial;
        }

        // line breaks
        if (ch == '\r')
        {
            popChar();
            if (front() == '\n')
                popChar();
            newLine();
            goto initial;
        }
        else if (ch == '\n' || ch == '\u2028' || ch == '\u2029')
        {
            popChar();
            newLine();
            goto initial;
        }
        else if (auto type = isSingleCharToken(ch))
        {
            popChar();
            return makeToken(type);
        }
        else if (isIdentifierStart(ch) || ch == '\\') // identifiers and keywords
        {
            // Replaces Unicode escape sequence
            //dstring name;
            alias name = _identifierBuffer;
            name.clear();
            if (ch == '\\')
            {
                next();
                if (ch != 'u')
                    throw new LexException(_file, _line, _col, `expected '\u' to make an Unicode escape sequence`);
                next();
                dchar escaped = cast(dchar)lexUnicodeEscape();
                if (!isIdentifierStart(escaped))
                    throw new LexException(_file, _line, _col, format(`identifiers cannot start with codepoint %s`, cast(int)escaped));
                name.put(escaped);
            }
            else
            {
                name.put(ch);
                next();
            }

            while(isIdentifierContinue(ch) || ch == '\\')
            {
                // Replaces Unicode escape sequence
                if (ch == '\\')
                {
                    next();
                    if (ch != 'u')
                        throw new LexException(_file, _line, _col, `expected '\u' to make an Unicode escape sequence`);
                    next();

                    dchar escaped = lexUnicodeEscape();

                    // Disabled until we support all Unicode ID_Start
                    if (!isIdentifierContinue(escaped))
                        throw new LexException(_file, _line, _col, format(`identifiers cannot contain codepoint %s`, cast(int)escaped));
                    name.put(escaped);
                }
                else
                {
                    name.put(ch);
                }
                next();
            }

            if (name.data == "true")
                return makeToken(TokenType.true_);
            else if (name.data == "false")
                return makeToken(TokenType.false_);
            else if (name.data == "null")
                return makeToken(TokenType.null_);
            throw new LexException(_file, _line, _col, format(`seen '%s' instead of 'true', 'false' or 'null'`, name.data));
        }
        else if (isDigit(ch)) // number literals
        {
            string value = lexDecimalLiteralStartingWithDigit();
            Token tok = makeToken(TokenType.number);
            tok.numberValue = tryConvertToNumber(tokenLine, tokenCol, value);
            return tok;
        }
        else if (ch == '\"') // string literals
        {
            _stringLiteralBuffer.clear();
            StringLiteral strLiteral;
            if (ch == '\"')
                strLiteral = StringLiteral.doubleQuote;
            else
                assert(false);

            void append(int codepoint)
            {
                // Surrogates are passed as is, Unicode consistency is verified at the end only
                if (isSurrogate(codepoint))
                    _stringLiteralBuffer.put( cast(ushort)codepoint );
                else
                {
                    wchar[2] buf;
                    size_t count = encode(buf, codepoint);
                    assert(count == 1 || count == 2);
                    _stringLiteralBuffer.put( buf[0] );
                    if (count > 1)
                        _stringLiteralBuffer.put( buf[1] );
                }
            }

            next();

            while (true)
            {
                if (ch == '\\') // escape sequence
                {
                    next();
                    switch(ch)
                    {
                        case 'b': append('\b'); next(); break;
                        case 't': append('\t'); next(); break;
                        case 'n': append('\n'); next(); break;
                        case 'v': append('\v'); next(); break;
                        case 'f': append('\f'); next(); break;
                        case 'r': append('\r'); next(); break;
                        case '$': append('$'); next(); break; // TODO: Not sure what the spec says about it
                        case '\"': append('\"'); next(); break;
                        case '\'': append('\''); next(); break;
                        case '`': append('`'); next(); break;
                        case '\\': append('\\'); next(); break;
                        case 'x':
                        {
                            // "hexadecimal" escape sequence eg: \xA0
                            int codepoint = 0;
                            next();
                            for (int digits = 0; digits < 2; ++digits)
                            {
                                codepoint = codepoint * 16 + hexCharToInt(_line, _col, ch);
                                next();
                            }
                            append(codepoint);
                            break;
                        }
                        case 'u':
                        {
                            // unicode escape sequence eg: \u1b46
                            next();
                            int codepoint = lexUnicodeEscape();

                            append(codepoint);
                            break;
                        }
                        case '\r': // CR or CRLF
                            next();
                            if (ch == '\n')
                                next();
                            newLine();
                            break;

                        case '\n':
                        case '\u2028':
                        case '\u2029':
                            next();
                            newLine();
                            break;

                        default:
                            if (isOctalDigit(ch))// && (strLiteral != StringLiteral.backQuote))
                            {
                                int octalValue = 0;
                                for (int i = 0; i < 3; ++i)
                                {
                                    octalValue = octalValue * 8 + (ch - '0');
                                    next();
                                    if (!isOctalDigit(ch))
                                        break;
                                }
                                if (octalValue >= 256)
                                    throw new LexException(_file, _line, _col, format("character %s is not allowed in octal escape sequence", octalValue));

                                append(cast(ushort)octalValue);
                            }
                            else
                                throw new LexException(_file, _line, _col, format(`unrecognized escape sequence \%s`, ch));
                    }
                }
                else if ( strLiteral == StringLiteral.doubleQuote && ch == '\"')
                {
                    // terminating quote or double-quote
                    popChar();
                    Token tok = makeToken(TokenType.string_);
                    tok.stringValue = cast(wstring)(_stringLiteralBuffer.data.idup); // TODO: use an allocator here
                    return tok;
                }
                else if (isLineSeparator(ch))
                {
                    throw new LexException(_file, _line, _col, "unexpected end of line in string literal");
                }
                else
                {
                    append(ch);
                    next();
                }
            }
        }     
        else
            throw new LexException(_file, _line, _col, format("unexpected char '%s' in input", ch));
    }


private:
    // State
    Input _input;
    int _position;
    int _line;
    int _col;
    string _file;

    Appender!(dchar[]) _identifierBuffer;
    Appender!(ushort[]) _stringLiteralBuffer;

    enum StringLiteral
    {
        doubleQuote,
    }

    void newLine()
    {
        _line += 1;
        _col = 1;
    }

    int binCharToInt(int line, int col, dchar ch)
    {
        if (ch >= '0' && ch <= '1')
            return ch - '0';
        else
            throw new LexException(_file, line, col, format("invalid character '%s' in binary number", ch));
    }

    static bool areOnlyOctalDigits(string s)
    {
        foreach(ch; s)
        {
            if (ch < '0' || ch > '7')
                return false;
        }
        return true;
    }

    int octalCharToInt(int line, int col, dchar ch)
    {
        if (ch >= '0' && ch <= 7)
            return ch - '0';
        else
            throw new LexException(_file, line, col, format("invalid character '%s' in octal number", ch));
    }

    int hexCharToInt(int line, int col, dchar ch)
    {
        if (ch >= '0' && ch <= '9')
            return ch - '0';
        else if (ch >= 'a' && ch <= 'f')
            return ch + 10 - 'a';
        else if (ch >= 'A' && ch <= 'F')
            return ch + 10 - 'A';
        else
            throw new LexException(_file, line, col, format("invalid character '%s' in hexadecimal number", ch));
    }

    static bool isLineSeparator(dchar ch)
    {
        return ch == '\n' || ch == '\r' || ch == '\u2028' || ch == '\u2029';
    }

    bool isWhitespace(dchar ch)
    {
        return ch in _whiteSpaceSet;
    }

    static bool isDigit(dchar ch)
    {
        return (ch >= '0' && ch <= '9');
    }

    static bool isOctalDigit(dchar ch)
    {
        return (ch >= '0' && ch <= '7');
    }

    bool isIdentifierStart(dchar ch)
    {
        return ch in _idStart;
    }

    bool isIdentifierContinue(dchar ch)
    {
        return ch in _idContinue;
    }

    double tryConvertToNumber(int line, int col, string s)
    {
        assert(s.length != 0);
        try
        {
            if (s.length > 2 && s[0] == '0')
            {
                if  (s[1] == 'x' || s[1] == 'X') // hexadecimal integer literal
                {
                    long n = 0;
                    foreach(int i, ch; s[2..$])
                        n = n * 16 + hexCharToInt(line, col + 2 + i, ch);
                    return cast(double)n;
                }
                else if (s[1] == 'b' || s[1] == 'B') // binary integer literal
                {
                    long n = 0;
                    foreach(int i, ch; s[2..$])
                        n = n * 2 + binCharToInt(line, col + 2 + i, ch);
                    return cast(double)n;
                }
                else if (areOnlyOctalDigits(s[1..$])) // octal integer literal
                {
                    long n = 0;
                    foreach(int i, ch; s[1..$])
                        n = n * 8 + octalCharToInt(line, col + 2 + i, ch);
                    return cast(double)n;
                }
                else
                    return to!double(s);
            }
            else
                return to!double(s);
        }
        catch(Exception e)
        {
            throw new LexException(_file, line, col, format(`"%s" is not a number`, s));
        }
    }

    // returns invalid if not a single char token, else the token type
    TokenType isSingleCharToken(dchar ch)
    {
        static immutable string singleCharTokens = ",:{}[]";
        static immutable TokenType[] correspondingTypes =
        [
            TokenType.comma,
            TokenType.colon,
            TokenType.openingBrace,
            TokenType.closingBrace,
            TokenType.openingBracket,
            TokenType.closingBracket,
        ];
        static assert(singleCharTokens.length == correspondingTypes.length);

        foreach(int i, char c; singleCharTokens)
        {
            if (c == ch)
                return correspondingTypes[i];
        }
        return TokenType.invalid;
    }
}

private:


__gshared CodepointSet _idStart;
__gshared CodepointSet _idContinue;
__gshared CodepointSet _whiteSpaceSet;


shared static this()
{
    _idStart = unicode.ID_Start | CodepointSet('$', '$'+1) | CodepointSet('_', '_'+1)
               | CodepointSet('a', 'z'+1, 'A', 'Z'+1);
    _idContinue = unicode.ID_Continue  | CodepointSet('$', '$'+1) | CodepointSet('_', '_'+1)
               | CodepointSet('a', 'z'+1, 'A', 'Z'+1) | CodepointSet('0', '9'+1);
    _whiteSpaceSet = CodepointSet('\u0009', '\u0009'+1)
                   | CodepointSet('\u000B', '\u000C'+1)
                   | CodepointSet('\u0020', '\u0020'+1)
                   | CodepointSet('\u00A0', '\u00A0'+1)
                   | CodepointSet('\uFEFF', '\uFEFF'+1)
                   | unicode.Space_Separator;
}


/// JSON parser
/// See_also: grammar at https://www.json.org/
struct Parser
{
public:
    void initialize(string source, string filename = null)
    {
        _lexer.initialize(source, filename);
        _file = filename;
        pop();
    }

    JSONValue parseValue() // element or value in grammar
    {

        if (see(TokenType.string_))
        {
            wstring str = peek().stringValue;
            pop();
            return JSONValue(str);
        }
        else if (see(TokenType.number))
        {
            double number = peek().numberValue;
            pop();
            return JSONValue(number);
        }
        else if (see(TokenType.openingBrace))
        {
            // parse object
            pop;
            JSONValue[string] members;

            while(true)
            {
                if (see(TokenType.closingBrace))
                {
                    pop;
                    return JSONValue(members);
                }

                if (see(TokenType.string_))
                {
                    // Convert to UTF-8
                    string key = to!string( peek().stringValue );
                    pop;
                    consume(TokenType.colon);
                    JSONValue value = parseValue();
                    members[key] = value; // TODO: dupe keys?
                }

                if (see(TokenType.comma))
                {
                    pop;
                    if (see(TokenType.closingBrace))
                        parseWarning("trailing comma in JSON object");
                }
                else if (see(TokenType.closingBrace))
                {
                }
                else
                    throw new ParseException(_file, currentPos,
                                             format("expected , or } in this JSON literal, but got %s", currentTokenString));               
            }
        }
        else if (see(TokenType.openingBracket))
        {
            // parse array
            pop;
            JSONValue[] items;

            while(true)
            {
                if (see(TokenType.closingBracket)) // TODO: allow "[,]" ?
                {
                    pop;
                    return JSONValue(items);
                }

                items ~= parseValue();

                if (see(TokenType.comma))
                {
                    pop;    
                    if (see(TokenType.closingBracket))
                        parseWarning("trailing comma in JSON array");
                }
                else if (see(TokenType.closingBracket))
                {
                }
                else
                    throw new ParseException(_file, currentPos,
                                             format("expected , or ] in this JSON array, but got %s", currentTokenString));             
            }
        }
        else if (see(TokenType.true_))
        {
            pop();
            return JSONValue(true);
        }
        else if (see(TokenType.false_))
        {
            pop();
            return JSONValue(false);
        }
        else if (see(TokenType.null_))
        {
            pop();
            return JSONValue(null);
        }
        else
            throw new ParseException(_file, currentPos,
                                     format("expected a JSON value but got %s", currentTokenString));
    }

    bool see(TokenType type)
    {
        return peek().type == type;
    }

    Pos currentPos()
    {
        return peek.pos;
    }

    // Gets current token
    Token peek()
    {
        return _token;
    }

    void pop()
    {
        _token = _lexer.nextToken();
    }

    string currentTokenString()
    {
        return tokenToString(peek().type);
    }

    void consume(TokenType type)
    {
        if (peek().type != type)
            throw new ParseException(_file, peek().pos,
                                     format("expected %s but got %s", tokenToString(type), currentTokenString));
        pop;
    }

    void ensureInputIsConsumed()
    {
        if (!see(TokenType.inputTerminator))
            throw new ParseException(_file, peek().pos,
                                     format("expected end of JSON file, but got %s", currentTokenString));
    }

    void parseWarning(string message)
    {
        auto pos = currentPos;
        string fmtMessage = format("%s(%d,%d): %s", _file, pos.line, pos.col, message);
        warningc(fmtMessage);
    }

private:
    Lexer!string _lexer;
    string _file;
    Token _token;
}

unittest
{
    bool parseValid(string input, int line = __LINE__)
    {
        try
        {
            parseJSONPermissive(input, format("unittest-L%s", line));
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    bool parseInvalid(string input, int line = __LINE__)
    {
        try
        {
            parseJSONPermissive(input, format("unittest-L%s", line));
            return false;
        }
        catch(Exception e)
        {
            return true;
        }
    }

    assert(parseValid(` { "simple": "object"} `));
    assert(parseValid(` { "simple": "object",} `)); // trailing comma in object
    assert(parseValid(` { "simple": [ 1, 2, 3, 4,]} `)); // trailing comma in array
    assert(parseInvalid(` { 4 } `));
    assert(parseValid(` { "field": 1.34 } `)); // float literal
    assert(parseValid(` { "field": 0.34e-4 } `));

    // TODO
    // currently failing tests
    //assert(parseValid(`42`)); // just a number
    //assert(parseValid(`"my-liter\n\nal"`)); // just a string literal

    assert(parseInvalid(`"unterminated`)); // just a string literal
}
